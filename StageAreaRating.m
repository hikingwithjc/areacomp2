% ---------------------------- GUI Description ---------------------------
% GENERAL DESCRIPTION:
% The StageAreaRating application is a graphical user interface used to
% analyze ADCP data and compute a relationship between channel area and 
% stage for an ADCP measurement.  This relationship, stage area rating, 
% can be used in the computation of discharge using index-velocity methods. 
% 
%------------------------------ GUI Updates ------------------------------
%
% - DSM/JGL  on 5/23/2012: Remove Select pushbutton, pdDelete always on.
%                          Removed data with all depths invalid for tfile.
%                          Changed version to 1.02
%
% - DSM/JGL  on 5/22/2012: Added prompt for number of header or label lines
%                          for csv file input. Changed version to 1.01
%
% - JGL edit on 4/03/2012: Added functionality for user to edit station or
%                          stage values directly from the table.  The
%                          edited values are converted to
%                          manualStationStage points.
%
% - JGL edit on 4/03/2012: Added functionality to fix duplicate station
%                          values. Duplicate station values causes the
%                          green fill color in the Cross Section Plot to
%                          not appear properly; appears like a stair-step.
%                          Upon importing data, if duplicate station values
%                          are found, the user is asked if they would like
%                          to adjust the station values by a very small
%                          number; 1.0 x 10^-6.  
%
% - JGL edit on 3/30/2012: Implemented new delete functionality that is
%                          based on data type instead on indices. 
%
% - JGL edit on 3/29/2012: Added a "Set Default" button to the "Stage Area
%                          Relationship Panel" so that the user can always
%                          set the default max and min of the table data
%                          and increment = 0.1 values.
%
% - JGL edit on 3/29/2012: Added new functions to help manage data:
%                          [StationStageMatrix] = combine_Data(ADCP,CSV,Manual) 
%                          [ADCP,CSV,Manual] = separate_Data(StationStageMatrix)  
%
% - JGL edit on 3/29/2012: Changed the original structure of handling the
%                          data. Instead of handling separate data matrices
%                          for ADCP and CSV data, the station and stage
%                          data is ADCP, CSV, and Manual entered is
%                          combined together into one matrix called Station
%                          Stage Matrix (SS).  This matrix also has a data
%                          type column to keep track of which data (ADCP,
%                          CSV, or Manual) the matrix contains.  The
%                          following are the data types:
%                          1 => ADCP
%                          2 => CSV
%                          3 => Manual
%                          This data type change made the "Delete" button
%                          functionality much easier to deal with instead 
%                          dealing with indices (as was done before).
%
% - JGL edit on 3/28/2012: Implemented new functionality to dataType to
%                          better organize and keep track of the 3 
%                          different data sets; adcpData, csvData,
%                          manualData.  This also fixes the issues in the "Delete"
%                          button.
% - JGL edit on 3/27/2012: Removed all references to a button called
%                          "Reset" which was used to delete manually 
%                          entered points.  User has this ability in the
%                          "Delete" button.
%
% - JGL edit on 3/27/2012: Added a flag, "csvStageFlag", to allow the user
%                          the ability to import elevations or depths in 
%                          the csv file. This affects how stage is
%                          computed.
%                          csvStage Flag = 1 => stage = gage ht + elevations
%                          csvStage Flag = 0 => stage = gage ht - depths
%
% - JGL edit on 3/27/2012: Added Station Number to the StationStage and
%                          StageArea output files
%
% - JGL edit on 3/26/2012: Added a warning box when user entered Max Stage
%                          is more than the bank elevation.
%
% - JGL edit on 3/26/2012: Turned "Resize" property "on" for the Stage Area
%                          Table figure ("View" -> "Stage Area Table"). Also,
%                          changed the size and position of the table
%                          inside the figure window.
%
% - JGL edit on 3/26/2012: Added a panel titled "User Information" where
%                          user can enter the Date, Station Number, User 
%                          Name, and Comments.  This data is saved with the 
%                          current workspace.
%
% - JGL edit on 3/26/2012: Added a function called "saveWorkspace(handles)".
%                          Used when user saves current work and used when
%                          user wants to save current work upon exiting the
%                          program => Prompt added to "File" -> "Exit"
%
% - JGL edit on 3/23/2012: Changed the menu to have following structure:
%                          File -> Import -> Sontek .mat File
%                                            TRDI Classic ASCII
%                                            CSV (comma separated)
%                                  Export -> Cross Section (CSV)
%                                            Stage Area Rating (CSV)
%                                  Open ->   Previous Work
%                                  Save ->   Current Work
%                                  Exit 
%
% - JGL edit on 3/23/2012: Changed GUI name from "StageAreaComp" to
%                          "AreaComp2". Kept same version # of 1.00
%
% - DSM edit on 3/20/2013: Changed tfile_nbs to tfileRead to improve speed,
%                           changed version to 1.05
%
% - DSM edit on 7/18/2013: Change GUI to be resizable. Added plot cross
% section function to pbComputeStage area to reset plot so that green
% shaded area is correct. When the max was lowered the top of the green
% shaded area was not being revised. Changed version to 1.06
% ------------------------------------------------------------------------

function varargout = StageAreaRating(varargin)
% STAGEAREARATING M-file for StageAreaRating.fig
%      STAGEAREARATING, by itself, creates a new STAGEAREARATING or raises the existing
%      singleton*.
%
%      H = STAGEAREARATING returns the handle to a new STAGEAREARATING or the handle to
%      the existing singleton*.
%
%      STAGEAREARATING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STAGEAREARATING.M with the given input arguments.
%
%      STAGEAREARATING('Property','Value',...) creates a new STAGEAREARATING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before StageAreaRating_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to StageAreaRating_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help StageAreaRating


% Last Modified by GUIDE v2.5 23-May-2012 12:32:56



% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @StageAreaRating_OpeningFcn, ...
                   'gui_OutputFcn',  @StageAreaRating_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before StageAreaRating is made visible.
function StageAreaRating_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no save args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to StageAreaRating (see VARARGIN)

% Choose default command line save for StageAreaRating
handles.save = hObject;


% --------------- Initialize GUI variables and interface -----------------

% Disable all check boxes and buttons to prevent user from entering any
% data or clicking any buttons without loading a data file
handles.version='AreaComp2 Version 1.07';
set(handles.figure1,'Name',handles.version);
set(0, 'DefaulttextInterpreter', 'none');
% User Information Panel
set(handles.txtBoxDate,'Enable','off');
set(handles.txtBoxStationNumber,'Enable','off');
set(handles.txtBoxUserName,'Enable','off');
set(handles.txtBoxComments,'Enable','off');
% ADCP Panel
set(handles.txtBoxGageHt,'Enable','off');
set(handles.txtBoxStationOffset,'Enable','off');
% CSV Panel
set(handles.txtBoxCSVGageHt,'Enable','off');
set(handles.txtBoxCSVStationOffset,'Enable','off');
% Station and Stage Table Panel
set(handles.tableStationStage,'Enable','off')
set(handles.pbAdd,'Enable','off');
set(handles.pbDelete,'Enable','off');


% Stage Area Relationship Panel
set(handles.txtBoxMaxStage,'Enable','off');
set(handles.txtBoxMinStage,'Enable','off');
set(handles.txtBoxIncrement,'Enable','off');
set(handles.pbComputeStageArea,'Enable','off');
set(handles.pbSetDefault,'Enable','off')
% Save Menu
set(handles.stationStageFile,'Enable','off');
set(handles.stageAreaRatingFile,'Enable','off');
set(handles.saveWorkspace,'Enable','off');
% View Menu
set(handles.stageAreaRatingTable,'Enable','off');
set(handles.projLine,'Enable','off');
set(handles.projPoints,'Enable','off');
set(handles.perpLines,'Enable','off');
set(handles.stageAreaRatingFile,'Enable','off');

% Initialize index handle for the 4 plotting and legend items in the 
% track plot
handles.idxTrackPlot(1:4) = false;
handles.idxTrackPlot(2) = true; 

% Initialize the ADCP and CSV data arrays
handles.btTrack = [];
handles.projxy = [];
handles.adcpDepth = [];
handles.adcpGageHt = [];
handles.adcpStationStageOrig = [];
handles.csvStationDepth = [];
handles.csvStationStageOrig = [];
handles.csvGageHt = [];
handles.adcpStationOffset = 0;
handles.csvStationOffset = 0;

% Station Stage (SS) matrix to hold the ADCP, CSV, and Manual station and 
% stage data.
handles.SS = [];

% Flag for CSV file; 0 => depths, 1 => elevations
handles.csvStageFlag = 0;

% Create the array to hold manually entered data
handles.manualStationStage = []; 

% Initialize data directory to current directory
handles.PathName=pwd; 

% Create a figure for the Stage Area Table
handles.f = 1;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes StageAreaRating wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = StageAreaRating_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning save args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line save from handles structure
varargout{1} = handles.save;




%=========================================================================
%                           FILE MENU
%=========================================================================


% ************************* IMPORT **************************************

% --------------------------------------------------------------------
function menu_File_Callback(hObject, eventdata, handles)
% hObject    handle to menu_File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 
% --------------------------------------------------------------------
function menu_Import_Callback(hObject, eventdata, handles)
% hObject    handle to menu_Import (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% LOAD SONTEK *.mat FILE
% --------------------------------------------------------------------
function load_sontek_Callback(hObject, eventdata, handles)
% hObject    handle to load_sontek (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% The function clear_gui asks is used to clear all the data in the GUI
% before opening a new data file.
handles = clear_gui(handles);

%-------------------------- Load Data ----------------------------------
% Ask user to select a SonTek matlab ADCP file, and load the file
set(handles.pbAdd,'Enable','off')
set(handles.pbDelete,'Enable','off')
if ishandle(handles.f)
    set(handles.f,'Visible','off');
end
set(handles.stageAreaRatingTable,'Enable','off');
[FileName,PathName] = uigetfile('*.mat','Select File',handles.PathName); 

% If user clicks "Cancel", uigetfile returns 0.
if isequal(FileName,0)
   return
end

% Concatenate PathName and FileName
FullName = strcat(PathName,FileName); 

% Make a handle to the adcpPathName to use when the user outputs a file. 
% The save will be placed directly in the directory referenced by the
% adcpPathName.
handles.PathName=PathName; 

% Load data file
load(FullName);

% Check if proper file by checking if the variable "System" exists
if exist('System','var')
    
    % Show filename in ADCP Panel
    set(handles.txtAdcpFilename,'String',FileName);
    
    %------------------------ Load Variables --------------------------------
    % Use only the moving boat portion of data by picking out the index number
    % corresponding to the moving boat.
    idx = find(System.Step == 3);
    
    % Initialize bottom track variables
    %       btTrack - x,y coordinates of adcp track
    btTrack = Summary.Track(idx,:);
    
    % Initialize depth variable.
    adcpDepth = Summary.Depth(idx,:);
    
    %------------------------ Compute Station and Stage ----------------------
    % Call compute_StationStage function
    %       inputs: (xcoord,ycoord)=> bottom track and depth
    %       outputs: Matrix of station, stage, and data type, project x and 
    %                y points, and default gage ht array of zeros.
    [adcpStationStageOrig, projxy, adcpGageHt] = compute_adcpStationStage(btTrack,adcpDepth);
    
    %---------------------- Assign Handles and Update ----------------------
    % Assign handles to local variables bottom track
    handles.btTrack = btTrack;
    handles.adcpDepth = adcpDepth;
    handles.projxy = projxy;
    handles.adcpGageHt = adcpGageHt;
    handles.adcpStationStageOrig = adcpStationStageOrig;
    
    % Set the adcpStationStageOrig data into the Station Stage (SS) matrix
    handles.SS = [handles.SS; adcpStationStageOrig];
    
    % Set ADCP gage height, Station Offset, track plot features on, table
    % features on, and stage area features on.
    % User Information Panel
    set(handles.txtBoxDate,'Enable','on');
    set(handles.txtBoxStationNumber,'Enable','on');
    set(handles.txtBoxUserName,'Enable','on');
    set(handles.txtBoxComments,'Enable','on');
    % ADCP Panel
    set(handles.txtBoxGageHt,'Enable','on')
    set(handles.txtBoxStationOffset,'Enable','on')
    % View Menu
    set(handles.projLine,'Enable','on')
    set(handles.projPoints,'Enable','on')
    set(handles.perpLines,'Enable','on')
    set(handles.projLine,'Checked','on');
    % Save Menu
    set(handles.stationStageFile,'Enable','on')
    set(handles.saveWorkspace,'Enable','on');
    % Station Stage Table and Panel
    set(handles.tableStationStage,'Enable','on')
    set(handles.pbAdd,'Enable','on')
    set(handles.pbDelete,'Enable','on')
    % Stage Area Relationship Panel
    set(handles.txtBoxMaxStage,'Enable','on')
    set(handles.txtBoxMinStage,'Enable','on')
    set(handles.txtBoxIncrement,'Enable','on')
    set(handles.txtBoxMaxStage,'String',num2str(max(handles.SS(:,2))))
    set(handles.txtBoxMinStage,'String',num2str(min(handles.SS(:,2))))
    set(handles.txtBoxIncrement,'String',num2str(0.1))
    set(handles.pbSetDefault,'Enable','on')
    set(handles.pbComputeStageArea,'Enable','on')
    
    % Plot the ADCP track
    plot_BottomTrack(btTrack, projxy, handles.idxTrackPlot,handles.axesTrack)
    
    % Call update_Data function to update arrays, Station and Stage table,
    % and Cross Section Plot.
    handles=update_Data(handles);
    
    % Update handles structure
    guidata(hObject, handles);
    
else
    errordlg('Improper input file. Please select proper SonTek *.mat file.')
    return
end


% LOAD TRDI CLASSIC ASCII FILE
% --------------------------------------------------------------------
function load_trdi_Callback(hObject, eventdata, handles) % DSM Added code
% hObject    handle to load_trdi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% The function clear_gui asks is used to clear all the data in the GUI
% before opening a new data file.
handles = clear_gui(handles);
set(handles.pbAdd,'Enable','off')
set(handles.pbDelete,'Enable','off')
if ishandle(handles.f)
    set(handles.f,'Visible','off');
end
set(handles.stageAreaRatingTable,'Enable','off');
[FileName,PathName] = uigetfile('*t.000;*ASC.txt','Select ASCII Output file for Processing',handles.PathName);

% If user clicks "Cancel", uigetfile returns 0.
if isequal(FileName,0)
   return
end

FullName = strcat(PathName,FileName);
handles.PathName=PathName;

% try
    % Load data file
    [Sup,Wat,Nav,Sensor,Q]=tfileRead(FullName,1,1);
    
    % Show filename in ADCP Panel
    set(handles.txtAdcpFilename,'String',FileName);
    

    
    % Compute weighted mean depth and set 4 nan back to nan
    depth=Nav.depth;
    w=1-depth./repmat(nansum(depth,2),1,4);
    temp=ones(size(depth));
    temp(~isnan(depth))=0;
    temp=sum(temp,2);
    idxValid=temp~=4;
    temp=depth(:,1)+depth(:,2)+depth(:,3)+depth(:,4);
    adcpDepth=nansum((depth.*w)./repmat(nansum(w,2),1,4),2);
    adcpDepth=adcpDepth(idxValid);   
    
    % Create btTrack array
    btTrack=[Nav.totDistEast(idxValid) Nav.totDistNorth(idxValid)];
    
    %------------------------ Compute Station and Stage ----------------------
    % Call compute_StationStage function
    %       inputs: xcoord,ycoord,depth,gageht
    %       save: array of station and stage values along with the projected
    %               x and y points.
    [adcpStationStageOrig, projxy, adcpGageHt] = compute_adcpStationStage(btTrack,adcpDepth);
    
    %---------------------- Assign Handles and Update ----------------------
    % Assign handles to local variables bottom track
    handles.btTrack = btTrack;
    handles.adcpDepth = adcpDepth;
    handles.projxy = projxy;
    handles.adcpGageHt = adcpGageHt;
    handles.adcpStationStageOrig = adcpStationStageOrig;

    % Set the adcpStationStageOrig data into the Station Stage (SS) matrix
    handles.SS = [handles.SS; adcpStationStageOrig];
    
    % Set ADCP gage height, Station Offset, track plot features on, table
    % features on, and stage area features on.
    % User Information Panel
    set(handles.txtBoxDate,'Enable','on');
    set(handles.txtBoxStationNumber,'Enable','on');
    set(handles.txtBoxUserName,'Enable','on');
    set(handles.txtBoxComments,'Enable','on');
    % ADCP Panel
    set(handles.txtBoxGageHt,'Enable','on')
    set(handles.txtBoxStationOffset,'Enable','on')
    % View Menu
    set(handles.projLine,'Enable','on')
    set(handles.projPoints,'Enable','on')
    set(handles.perpLines,'Enable','on')
    set(handles.projLine,'Checked','on');
    % Save Menu
    set(handles.stationStageFile,'Enable','on')
    set(handles.saveWorkspace,'Enable','on');
    % Station Stage Table and Panel
    set(handles.tableStationStage,'Enable','on')
    set(handles.pbAdd,'Enable','on')
    set(handles.pbDelete,'Enable','on')
    % Stage Area Relationship Panel
    set(handles.txtBoxMaxStage,'Enable','on')
    set(handles.txtBoxMinStage,'Enable','on')
    set(handles.txtBoxIncrement,'Enable','on')
    set(handles.txtBoxMaxStage,'String',num2str(max(handles.SS(:,2))))
    set(handles.txtBoxMinStage,'String',num2str(min(handles.SS(:,2))))
    set(handles.txtBoxIncrement,'String',num2str(0.1))
    set(handles.pbSetDefault,'Enable','on')
    set(handles.pbComputeStageArea,'Enable','on')
    
    % Plot the ADCP track
    plot_BottomTrack(btTrack, projxy, handles.idxTrackPlot, handles.axesTrack)
    
    % Call update_Data function to update arrays, Station and Stage table,
    % and Cross Section Plot.
    handles=update_Data(handles);
    
    % Update handles structure
    guidata(hObject, handles);

% catch ME
%     msg = ME.message; 
%     errordlg('Improper input file. Please select proper TRDI Classic ASCII *.txt file.')
%     return
% end

% LOAD CSV
% --------------------------------------------------------------------
function load_csv_Callback(hObject, eventdata, handles)
% hObject    handle to load_csv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% The function clear_gui asks is used to clear all the data in the GUI
% before opening a new data file.
handles = clear_gui(handles);
set(handles.pbAdd,'Enable','off')
set(handles.pbDelete,'Enable','off')
if ishandle(handles.f)
    set(handles.f,'Visible','off');
end
set(handles.stageAreaRatingTable,'Enable','off');
%-------------------------- Load Data ----------------------------------
% Ask user to select a CSV file, and load the file
[csvFileName,csvPathName] = uigetfile('*.txt; *.csv','Select File',handles.PathName);

% If user clicks "Cancel", uigetfile returns 0.
if isequal(csvFileName,0)
   return
end

csvFullName = strcat(csvPathName,csvFileName);
handles.PathName=csvPathName; 

userLineNo=inputdlg('Enter number of header or label lines in csv file: ',...
    'CSV File',1,{'0'});
userLineNo=str2double(userLineNo{1});

% Load data file
% Read a comma separated file starting at row 1, column 0.  *Note: dlmread
% starts its index at 0.

try 
    % Start reading at the 2nd row, and 1st column (1,0)
    csvFile = dlmread(csvFullName,',',userLineNo,0);
    
    % Show filename in CSV Panel
    set(handles.txtCSVFilename,'String',csvFileName);
    
    %------------------------ Load Variables ------------------------------
    % Assign the local variables to the data.
    csvStationDepth = csvFile;
    
    %------------------------ Elevations or Depths ------------------------
    % Ask user if the csvFile contains elevations or depths.  This
    % determines how stage is calculated. 
    % csvStageFlag = 1 => elevations => stage = gage ht + elevations
    % csvStageFlag = 0 => depths => stage = gage ht - depths
    
    c=questdlg('Does the CSV file contain depths or elevations?','Depths or Elevations?','Depths','Elevations','Depths');
    if strcmp(c,'Elevations')
        handles.csvStageFlag = 1;
    end
    
    %------------------------ Compute Stages ------------------------------
    % Call compute_csvStationStage to calculate and update csv station and
    % stages.
    [csvStationStageOrig, csvGageHt] = compute_csvStationStage(csvStationDepth, handles);
    
    %---------------------- Assign Handles and Update ----------------------
    % Assign handles to local variables
    handles.csvStationDepth = csvStationDepth;
    handles.csvStationStageOrig = csvStationStageOrig;
    handles.csvGageHt = csvGageHt;    

    % Set the csvStationStageOrig data into the Station Stage (SS) matrix
    handles.SS = [handles.SS; csvStationStageOrig];
    
    % Set CSV gage height, Station Offset, track plot features on, table
    % features on, and stage area features on.
    % User Information Panel
    set(handles.txtBoxDate,'Enable','on');
    set(handles.txtBoxStationNumber,'Enable','on');
    set(handles.txtBoxUserName,'Enable','on');
    set(handles.txtBoxComments,'Enable','on');
    % CSV Panel
    set(handles.txtBoxCSVGageHt,'Enable','on')
    set(handles.txtBoxCSVStationOffset,'Enable','on')
    % View Menu
    set(handles.projLine,'Enable','on')
    set(handles.projPoints,'Enable','on')
    set(handles.perpLines,'Enable','on')
    % save Menu
    set(handles.stationStageFile,'Enable','on')
    set(handles.saveWorkspace,'Enable','on');
    % Station Stage Table and Panel
    set(handles.tableStationStage,'Enable','on')
    set(handles.pbAdd,'Enable','on')
    set(handles.pbDelete,'Enable','on')
    % Stage Area Relationship Panel
    set(handles.txtBoxMaxStage,'Enable','on')
    set(handles.txtBoxMinStage,'Enable','on')
    set(handles.txtBoxIncrement,'Enable','on')
    set(handles.txtBoxMaxStage,'String',num2str(max(handles.SS(:,2))))
    set(handles.txtBoxMinStage,'String',num2str(min(handles.SS(:,2))))
    set(handles.txtBoxIncrement,'String',num2str(0.1))
    set(handles.pbSetDefault,'Enable','on')
    set(handles.pbComputeStageArea,'Enable','on')
    
    % Call update_Data function to update arrays, Station and Stage table, and
    % Cross Section Plot.
    handles=update_Data(handles);
    
    % Update handles structure
    guidata(hObject, handles);
    
catch ME
    msg = ME.message; 
    errordlg('Improper input file. Please select proper comma separated value *.txt,*.csv file containing 2 columns of data; Station,Depth.')
    return
end


% LOAD AREACOMP1.3 *.act FILE
% --------------------------------------------------------------------
function actFile_Callback(hObject, eventdata, handles)
% hObject    handle to actFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% The function clear_gui asks is used to clear all the data in the GUI
% before opening a new data file.

handles = clear_gui(handles);

set(handles.pbAdd,'Enable','off')
set(handles.pbDelete,'Enable','off')
if ishandle(handles.f)
    set(handles.f,'Visible','off');
end
set(handles.stageAreaRatingTable,'Enable','off');
%-------------------------- Load Data ----------------------------------
% Ask user to select a ACT file, and load the file
[actFileName,actPathName] = uigetfile('*.act','Select File',handles.PathName);

% If user clicks "Cancel", uigetfile returns 0.
if isequal(actFileName,0)
   return
end

actFullName = strcat(actPathName,actFileName);
handles.PathName=actPathName; 

try 
    
    % Read header as strings
    fileID = fopen(actFullName);
    title = fgetl(fileID);
    station = fgetl(fileID);
    minStage = fgetl(fileID);
    maxStage = fgetl(fileID);
    increment = fgetl(fileID);
    gageHt = fgetl(fileID);
    
    stationStr = station(10:end);
    % Convert variables of interest to numbers
    minStage = str2double(minStage(12:end));
    maxStage = str2double(maxStage(12:end));
    increment = str2double(increment(14:end));
    gageHt = -1.*str2double(gageHt(10:end));
    
    fclose(fileID);
    
    % Read Station Elevation data
    stationElev = dlmread(actFullName,'\t',7,0);
        
    % Show filename in CSV Panel
    set(handles.txtCSVFilename,'String',actFileName);
    
    % Show a help dialog telling user that the AREACOMP1.3 file will be
    % treated in a similar fashion as the CSV file.
    h = helpdlg('**NOTE**: The AreaComp1.3 file will be treated like a CSV file.',...
                'AreaComp1.3 File Import');
    uiwait(h);
    %------------------------ Load Variables ------------------------------
    % Assign the local variables to the data.
    actStationElev = stationElev;
    
    %------------------------ Compute Stages ------------------------------
    % Call compute_actStationStage to calculate and update csv station and
    % stages.
    [actStationStageOrig, actGageHt] = compute_actStationStage(actStationElev, gageHt, handles);
    
    %---------------------- Assign Handles and Update ----------------------
    % Assign handles to local variables
    handles.csvStationDepth = actStationElev;
    handles.csvStationStageOrig = actStationStageOrig;
    handles.csvGageHt = actGageHt;    

    % Set the csvStationStageOrig data into the Station Stage (SS) matrix
    handles.SS = [handles.SS; actStationStageOrig];
    
    % Set CSV gage height, Station Offset, track plot features on, table
    % features on, and stage area features on.
    % User Information Panel
    set(handles.txtBoxDate,'Enable','on');
    set(handles.txtBoxStationNumber,'Enable','on');
    set(handles.txtBoxStationNumber,'String',stationStr,'HorizontalAlignment','left');
    set(handles.txtBoxUserName,'Enable','on');
    set(handles.txtBoxComments,'Enable','on');
    % CSV Panel
    set(handles.txtBoxCSVGageHt,'Enable','on');
    set(handles.txtBoxCSVGageHt,'String',num2str(gageHt));
    set(handles.txtBoxCSVStationOffset,'Enable','on');
    % View Menu
    set(handles.projLine,'Enable','on');
    set(handles.projPoints,'Enable','on');
    set(handles.perpLines,'Enable','on');
    % save Menu
    set(handles.stationStageFile,'Enable','on');
    set(handles.saveWorkspace,'Enable','on');
    % Station Stage Table and Panel
    set(handles.tableStationStage,'Enable','on');
    set(handles.pbAdd,'Enable','on');
    set(handles.pbDelete,'Enable','on');
    % Stage Area Relationship Panel
    set(handles.txtBoxMaxStage,'Enable','on');
    set(handles.txtBoxMinStage,'Enable','on');
    set(handles.txtBoxIncrement,'Enable','on');
    set(handles.txtBoxMaxStage,'String',num2str(maxStage));
    set(handles.txtBoxMinStage,'String',num2str(minStage));
    set(handles.txtBoxIncrement,'String',num2str(increment));
    set(handles.pbSetDefault,'Enable','on');
    set(handles.pbComputeStageArea,'Enable','on');
    
    % Call update_Data function to update arrays, Station and Stage table, and
    % Cross Section Plot.
    handles=update_Data(handles);
    
    % Update handles structure
    guidata(hObject, handles);
    
catch ME
    msg = ME.message; 
    errordlg('Improper input file. Please select proper AreaComp1.3 *.act file.')
    return
end



% ****************************** EXPORT ***********************************

% --------------------------------------------------------------------
function menu_Export_Callback(hObject, eventdata, handles)
% hObject    handle to menu_Export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% STATION STAGE FILE
% --------------------------------------------------------------------
function stationStageFile_Callback(hObject, eventdata, handles)
% hObject    handle to stationStageFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Create an save file and write data to it.
userStationNumber = get(handles.txtBoxStationNumber,'String');
outputStr = strcat('StationStage_out_',userStationNumber,'.txt');
handles.stationStageOut=[handles.PathName outputStr];
fullname = handles.stationStageOut;
outputfile = fopen(fullname, 'w');
fprintf(outputfile,[handles.version '\r\n' ' \r\n']);
fprintf(outputfile,[date '\r\n' ' \r\n']);
fprintf(outputfile, 'Station,Stage\r\n');

% Get the current data from the table
tableData = get(handles.tableStationStage,'Data');

stationOut = tableData(:,1);
stageOut = tableData(:,2);

for i = 1:length(stationOut)
    fprintf(outputfile, '%4.5f,%4.5f\r\n', stationOut(i), stageOut(i));
end

fclose(outputfile);

% Update handles structure
guidata(hObject, handles);

% STAGE AREA FILE
% --------------------------------------------------------------------
function stageAreaRatingFile_Callback(hObject, eventdata, handles)
% hObject    handle to stageAreaRatingFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Create an save file and write data to it.
userStationNumber = get(handles.txtBoxStationNumber,'String');
outputStr = strcat('StationArea_out_',userStationNumber,'.txt');
handles.stageAreaOut=[handles.PathName outputStr];
fullname = handles.stageAreaOut;
outputfile = fopen(fullname, 'w');
fprintf(outputfile,[handles.version '\r\n' ' \r\n']);
fprintf(outputfile,[date '\r\n' ' \r\n']);
fprintf(outputfile, ['Stage,Area' '\r\n']);

% Get the current data 
data = [handles.stageIncrements handles.areas];

stageOut = data(:,1);
areaOut = data(:,2);

for i = 1:length(stageOut)
    fprintf(outputfile, '%4.5f,%4.5f\r\n', stageOut(i), areaOut(i));
end

fclose(outputfile);

% Update handles structure
guidata(hObject, handles);



% ****************************** OPEN *************************************

% --------------------------------------------------------------------
function menu_Open_Callback(hObject, eventdata, handles)
% hObject    handle to menu_Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% OPEN PREVIOUS WORKSPACE
% --------------------------------------------------------------------
function loadWorkspace_Callback(hObject, eventdata, handles)
% hObject    handle to loadWorkspace (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% The function clear_gui asks is used to clear all the data in the GUI
% before opening a new data file.
handles = clear_gui(handles);

% Ask user to select a workspace matlab file, and load the file
[FileName,PathName] = uigetfile('*.mat','Select Previous Workspace File',handles.PathName); 

% If user clicks "Cancel", uigetfile returns 0.
if isequal(FileName,0)
   return
end

FullName = strcat(PathName,FileName); 
% Make a handle to the adcpPathName to use when the user outputs a file. 
% The save will be placed directly in the directory referenced by the
% adcpPathName.
handles.PathName=PathName; 

% Load data file
load(FullName);

% Check if proper file by checking if the variable "btTrack" exists
if exist('btTrack','var')

%---------------------- Set and Assign Handles---------------------------
    % Original data
    handles.btTrack = btTrack;
    handles.projxy = projxy;
    handles.adcpDepth = adcpDepth;
    handles.adcpStationStageOrig = adcpStationStageOrig;
    handles.csvStationDepth = csvStationDepth;
    handles.csvStationStageOrig = csvStationStageOrig;
    % User edited station stage matrices
    handles.SS = SS;
    handles.adcpGageHt = adcpGageHt;
    handles.csvGageHt = csvGageHt;
    handles.adcpStationOffset = adcpStationOffset;
    handles.csvStationOffset = csvStationOffset;
    
    % Set the text boxes
    if ~isempty(handles.adcpGageHt)
        set(handles.txtBoxGageHt,'String',num2str(adcpGageHt(1)));
        set(handles.txtBoxStationOffset,'String', num2str(adcpStationOffset));
    elseif ~isempty(handles.csvGageHt)
        set(handles.txtBoxCSVGageHt,'String',num2str(csvGageHt(1)));
        set(handles.txtBoxCSVStationOffset,'String',num2str(csvStationOffset));
    end
    
    set(handles.txtAdcpFilename,'String',adcpFileName);
    set(handles.txtCSVFilename,'String',csvFileName);
    set(handles.txtBoxDate,'String',userDate);
    set(handles.txtBoxStationNumber,'String',userStationNumber);
    set(handles.txtBoxUserName,'String',userName);
    set(handles.txtBoxComments,'String',userComments);
    
    % Plotting variables
    handles.idxTrackPlot = idxTrackPlot;
    
    % View Menu
    set(handles.projLine,'Enable','on')
    set(handles.projPoints,'Enable','on')
    set(handles.perpLines,'Enable','on')
    % Save Menu
    set(handles.stationStageFile,'Enable','on')
    set(handles.saveWorkspace,'Enable','on');
   
    % User Information Panel
    set(handles.txtBoxDate,'Enable','on');
    set(handles.txtBoxStationNumber,'Enable','on');
    set(handles.txtBoxUserName,'Enable','on');
    set(handles.txtBoxComments,'Enable','on');
    % Station Stage Table and Panel
    set(handles.tableStationStage,'Enable','on')
    set(handles.pbAdd,'Enable','on')
    % Stage Area Relationship Panel
    set(handles.txtBoxMaxStage,'Enable','on')
    set(handles.txtBoxMinStage,'Enable','on')
    set(handles.txtBoxIncrement,'Enable','on')
    set(handles.pbComputeStageArea,'Enable','on')
    set(handles.pbSetDefault,'Enable','on')
    
    % Separate the data for plotting
    [adcpStationStage, csvStationStage, ~] = separate_Data(SS);
    
    % If ADCP data exists so enable proper text boxes and plot bottom track
    if ~isempty(adcpStationStage)
        % ADCP Panel
        set(handles.txtBoxGageHt,'Enable','on')
        set(handles.txtBoxStationOffset,'Enable','on')
        
        % Plot the ADCP track
        plot_BottomTrack(btTrack, projxy, handles.idxTrackPlot,handles.axesTrack)
    
    % If CSV data exists so enable proper text boxes
    elseif ~isempty(csvStationStage)
        % CSV Panel
        set(handles.txtBoxCSVGageHt,'Enable','on')
        set(handles.txtBoxCSVStationOffset,'Enable','on')        
    end
    
    % Call update_Data function
    update_Data(handles);
    
    % Set the following text boxes after updating because in the
    % update_Data function ,the MaxStage, MinStage, and Increment are
    % computed as max(StationStageMatrix), min(StationStageMatrix), and 0.1
    % respectively.  This overrides that computation so show user edits.
    set(handles.txtBoxMaxStage,'String',num2str(maxStage));
    set(handles.txtBoxMinStage,'String',num2str(minStage));
    set(handles.txtBoxIncrement,'String',num2str(increment));
    
    
    % Update handles structure
    guidata(hObject, handles);
else
    errordlg('Improper input file. Please select proper Previous Workspace *.mat file.')
    return
end



% ****************************** SAVE *************************************

% --------------------------------------------------------------------
function menu_Save_Callback(hObject, eventdata, handles)
% hObject    handle to menu_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% SAVE CURRENT WORKSPACE
% --------------------------------------------------------------------
function saveWorkspace_Callback(hObject, eventdata, handles)
% hObject    handle to saveWorkspace (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Call saveWorkspace function            
handles = saveWorkspace(handles);

% Update handles structure
guidata(hObject, handles);



% ****************************** EXIT *************************************

% EXIT PROGRAM
% --------------------------------------------------------------------
function menu_Exit_Callback(hObject, eventdata, handles)
% hObject    handle to menu_Exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% The function clear_gui asks is used to clear all the data in the GUI
% before opening a new data file.
c=questdlg('Do you want to save current work before exiting?',...
           'Save Current Work Before Exiting?','Yes','No','Yes');
if strcmp(c,'Yes')
    handles = saveWorkspace(handles);
    
    % Update handles structure
    guidata(hObject, handles);
    
    % Close program
    close all;
else
    close all;
end



%=========================================================================
%                              VIEW MENU
%=========================================================================

% --------------------------------------------------------------------
function view_Callback(hObject, eventdata, handles)
% hObject    handle to view (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% VIEW STAGE AREA RATING TABLE
% --------------------------------------------------------------------
function stageAreaRatingTable_Callback(hObject, eventdata, handles)
% hObject    handle to stageAreaRatingTable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% View Stage Area Rating Table
% --------------------------------------------------------------------

% Setup table data and parameters
columnNames = {'Stage','Area'};
data = [handles.stageIncrements handles.areas];
data=sortrows(data,1);

%If figure is not available, create a new figure.
if ~ishandle(handles.f)
    handles.f = figure('Position',[500 500 220 420],'MenuBar','none','Resize','on',...
                       'NumberTitle','off','Visible','off');
    uitable('Parent',handles.f,'Data',data,'ColumnName',columnNames,'Units','pixels',...
        'Position',[10 10 200 400],'ColumnWidth',{70 70},'ColumnFormat',{'bank','bank'},'Units','normalized');
else
   % If figure is open overwrite the table in the current position
   set(handles.f,'Visible','on');
   ft1=get(handles.f,'Children');
   set(ft1,'Units','pixels');
   tablePos=get(ft1,'Position');
   delete(ft1);
   uitable('Parent',handles.f,'Data',data,'ColumnName',columnNames,'Units','pixels',...
        'Position',tablePos,'ColumnWidth',{70 70},'ColumnFormat',{'bank','bank'},'Units','normalized');
end

set(handles.f,'Visible','on');
% Update handles structure
guidata(hObject, handles);

% --------------------------------------------------------------------
function trackPlotFeatures_Callback(hObject, eventdata, handles)
% hObject    handle to trackPlotFeatures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% VIEW PROJECTION LINE
% --------------------------------------------------------------------
function projLine_Callback(hObject, eventdata, handles)
% hObject    handle to projLine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get the 'Checked' toggle between the on and off
state = get(hObject,'Checked');
state = strcmp(state,'on');

% If the state is off (state == 0), then turn it on and set the index for
% the track plot to true in order to plot.
if state == 0;
    set(hObject,'Checked','on')
    handles.idxTrackPlot(2) = true;
    plot_BottomTrack(handles.btTrack, handles.projxy, handles.idxTrackPlot,handles.axesTrack)
else 
    set(hObject,'Checked','off')
    handles.idxTrackPlot(2) = false;
    plot_BottomTrack(handles.btTrack, handles.projxy, handles.idxTrackPlot,handles.axesTrack)
end

% Update handles structure
guidata(hObject, handles);

% VIEW PROJECTION POINTS
% --------------------------------------------------------------------
function projPoints_Callback(hObject, eventdata, handles)
% hObject    handle to projPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get the 'Checked' toggle between the on and off
state = get(hObject,'Checked');
state = strcmp(state,'on');

% If the state is off (state == 0), then turn it on and set the index for
% the track plot to true in order to plot.
if state == 0;
    set(hObject,'Checked','on')
    handles.idxTrackPlot(3) = true;
    plot_BottomTrack(handles.btTrack, handles.projxy, handles.idxTrackPlot,handles.axesTrack)
else 
    set(hObject,'Checked','off')
    handles.idxTrackPlot(3) = false;
    plot_BottomTrack(handles.btTrack, handles.projxy, handles.idxTrackPlot,handles.axesTrack)
end

% Update handles structure
guidata(hObject, handles);

% VIEW PERPENDICULAR LINES
% --------------------------------------------------------------------
function perpLines_Callback(hObject, eventdata, handles)
% hObject    handle to perpLines (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get the 'Checked' toggle between the on and off
state = get(hObject,'Checked');
state = strcmp(state,'on');

% If the state is off (state == 0), then turn it on and set the index for
% the track plot to true in order to plot.
if state == 0;
    set(hObject,'Checked','on')
    handles.idxTrackPlot(4) = true;
    plot_BottomTrack(handles.btTrack, handles.projxy, handles.idxTrackPlot,handles.axesTrack)
else 
    set(hObject,'Checked','off')
    handles.idxTrackPlot(4) = false;
    plot_BottomTrack(handles.btTrack, handles.projxy, handles.idxTrackPlot,handles.axesTrack)
end

% Update handles structure
guidata(hObject, handles);




%=========================================================================
%                                PANELS
%=========================================================================

% USER INFORMATION PANEL
%-------------------------------------------------------------------------
function txtBoxDate_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxDate as text
%        str2double(get(hObject,'String')) returns contents of txtBoxDate as a double

% Get the string that the user enters
userDate = str2double(get(handles.txtBoxDate,'String'));

% Assign handles back to updated variables
handles.userDate = date;

% Update handles structure
guidata(hObject, handles);


function txtBoxStationNumber_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxStationNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxStationNumber as text
%        str2double(get(hObject,'String')) returns contents of txtBoxStationNumber as a double

% Get the string that the user enters
userStationNumber = str2double(get(handles.txtBoxStationNumber,'String'));

% Assign handles back to updated variables
handles.userStationNumber = userStationNumber;

% Update handles structure
guidata(hObject, handles);


function txtBoxUserName_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxUserName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxUserName as text
%        str2double(get(hObject,'String')) returns contents of txtBoxUserName as a double

% Get the string that the user enters
userName = str2double(get(handles.txtBoxUserName,'String'));

% Assign handles back to updated variables
handles.userName = userName;

% Update handles structure
guidata(hObject, handles);


function txtBoxComments_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxComments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxComments as text
%        str2double(get(hObject,'String')) returns contents of txtBoxComments as a double

% Get the string that the user enters
userComments = str2double(get(handles.txtBoxComments,'String'));

% Assign handles back to updated variables
handles.userComments = userComments;

% Update handles structure
guidata(hObject, handles);


% ADCP PANEL
%-------------------------------------------------------------------------
function txtBoxGageHt_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxGageHt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxGageHt as text
%        str2double(get(hObject,'String')) returns contents of txtBoxGageHt as a double

% Make sure user enters a numeric value
userGageHt = str2double(get(handles.txtBoxGageHt,'String'));
if isnan(userGageHt)
    warndlg('Input is not numeric. Please enter a numeric value.');
    set(handles.txtBoxGageHt,'String',0);
    %set(handles.txtBoxGageHt,'String',handles.adcpGageHt(1));
else
    % ------------------------ Assign Local Variables----------------------
    SS = handles.SS;
    adcpGageHt = handles.adcpGageHt;
    
    %----------------------- Adjust Stages to Gage Ht ---------------------
    % Get the adcpStationStage data
    [adcpStationStage, csvStationStage, manualStationStage] = separate_Data(SS);
    
    % Replicate the userGageHt to the same size as the adcpStationStage
    userGageHt = repmat(userGageHt,[size(adcpStationStage,1), 1]);
    
    % Compute difference between previous gage ht and current stage values
    depth = (adcpGageHt - adcpStationStage(:,2));
    
    % Adjust the stage values 
    stage = userGageHt - depth;
    
    % Assign new stage values to adcpStationStage
    adcpStationStage(:,2) = stage;
    
    % Add new stage values to the StationStageMatrix
    newSS = combine_Data(adcpStationStage,csvStationStage,manualStationStage);
    
    %------------------- Assign Handles to Local Variables----------------
    handles.adcpGageHt = userGageHt;
    handles.SS = newSS;
     
    % Call update_Data function to update arrays, Station and Stage table, and
    % Cross Section Plot.
    handles=update_Data(handles);
    
    % Set the new default values in text boxes
    set(handles.txtBoxMaxStage,'String',num2str(max(handles.SS(:,2))))
    set(handles.txtBoxMinStage,'String',num2str(min(handles.SS(:,2))))
    set(handles.txtBoxIncrement,'String',num2str(0.1))
    
    % Update handles structure
    guidata(hObject, handles);
end


function txtBoxStationOffset_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxStationOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxStationOffset as text
%        str2double(get(hObject,'String')) returns contents of txtBoxStationOffset as a double

% Make sure user enters a numeric value
userStationOffset = str2double(get(handles.txtBoxStationOffset,'String'));
if isnan(userStationOffset)
    warndlg('Input is not numeric. Please enter a numeric value.');
    set(handles.txtBoxStationOffset,'String',0);
else
    % ------------------------ Assign Local Variables----------------------
    SS = handles.SS;
    adcpStationOffset = handles.adcpStationOffset;
    
    %----------------------- Adjust Stations ---------------------
    % Get the adcpStationStage data
    [adcpStationStage, csvStationStage, manualStationStage] = separate_Data(SS);
    
    % Compute difference between previous station and current station values 
    offset = abs(adcpStationOffset - adcpStationStage(:,1));
    
    % Adjust the station values (first column of station stage matrix)
    station = offset + userStationOffset;
    
    % Assign new stage values to adcpStationStage
    adcpStationStage(:,1) = station;
    
    % Add new stage values to the StationStageMatrix
    newSS = combine_Data(adcpStationStage,csvStationStage,manualStationStage);
    
    %------------------- Assign Handles to Local Variables ----------------
    handles.SS = newSS;
    handles.adcpStationOffset = userStationOffset;
    
    % Call update_Data function to update arrays, Station and Stage table, and
    % Cross Section Plot.
    handles=update_Data(handles);
    
    % Update handles structure
    guidata(hObject, handles);
end


% CSV PANEL
%-------------------------------------------------------------------------
function txtBoxCSVGageHt_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxCSVGageHt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxCSVGageHt as text
%        str2double(get(hObject,'String')) returns contents of txtBoxCSVGageHt as a double

% Make sure user enters a numeric value
userGageHt = str2double(get(handles.txtBoxCSVGageHt,'String'));
if isnan(userGageHt)
    warndlg('Input is not numeric. Please enter a numeric value.');
    set(handles.txtBoxCSVGageHt,'String',0);
else
    % ------------------------ Assign Local Variables----------------------
    SS = handles.SS;
    csvGageHt = handles.csvGageHt;
    
    %----------------------- Adjust Stages to Gage Ht ---------------------
    % Get the adcpStationStage data
    [adcpStationStage, csvStationStage, manualStationStage] = separate_Data(SS);
    
    % Replicate the userGageHt to the same size as the csvStationStage
    userGageHt = repmat(userGageHt,[size(csvStationStage,1), 1]);
    
    % Compute difference between previous gage ht and current stage values
    depth = (csvGageHt - csvStationStage(:,2));
    
    % Adjust the stage values 
    stage = userGageHt - depth;
    
    % Assign new stage values to csvStationStage
    csvStationStage(:,2) = stage;
    
    % Add new stage values to the StationStageMatrix
    newSS = combine_Data(adcpStationStage,csvStationStage,manualStationStage);
    
    %------------------- Assign local variables to Handles----------------
    handles.csvGageHt = userGageHt;
    handles.SS = newSS;
     
    % Call update_Data function to update arrays, Station and Stage table, and
    % Cross Section Plot.
    handles=update_Data(handles);
    
    % Set the new default values in text boxes
    set(handles.txtBoxMaxStage,'String',num2str(max(handles.SS(:,2))))
    set(handles.txtBoxMinStage,'String',num2str(min(handles.SS(:,2))))
    set(handles.txtBoxIncrement,'String',num2str(0.1))
    
    % Update handles structure
    guidata(hObject, handles);
end

function txtBoxCSVStationOffset_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxCSVStationOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxCSVStationOffset as text
%        str2double(get(hObject,'String')) returns contents of txtBoxCSVStationOffset as a double

% Make sure user enters a numeric value
userStationOffset = str2double(get(handles.txtBoxCSVStationOffset,'String'));
if isnan(userStationOffset)
    warndlg('Input is not numeric. Please enter a numeric value.');
    set(handles.txtBoxCSVStationOffset,'String',0);
else
    % ------------------------ Assign Local Variables----------------------
    SS = handles.SS;
    csvStationOffset = handles.csvStationOffset;
    
    %----------------------- Adjust Stations ---------------------
    % Get the adcpStationStage data
    [adcpStationStage, csvStationStage, manualStationStage] = separate_Data(SS);
    
    % Compute difference between previous station and current station values 
    offset = abs(csvStationOffset - csvStationStage(:,1));
    
    % Adjust the station values (first column of station stage matrix)
    station = offset + userStationOffset;
    
    % Assign new stage values to adcpStationStage
    csvStationStage(:,1) = station;
    
    % Add new stage values to the StationStageMatrix
    newSS = combine_Data(adcpStationStage,csvStationStage,manualStationStage);
    
    %------------------- Assign Handles to Local Variables ----------------
    handles.SS = newSS;
    handles.csvStationOffset = userStationOffset;
    
    % Call update_Data function to update arrays, Station and Stage table, and
    % Cross Section Plot.
    handles=update_Data(handles);
    
    % Update handles structure
    guidata(hObject, handles);
end


% STATION STAGE TABLE PANEL
%-------------------------------------------------------------------------
% --- Executes when entered data in editable cell(s) in tableStationStage.
function tableStationStage_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tableStationStage (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

%-------------------------Assign Local Variables to Handles----------------
SS = handles.SS;
adcpGageHt = handles.adcpGageHt;
csvGageHt = handles.csvGageHt;
manualStationStage = handles.manualStationStage;

%-------------------------Check Input and Update Data --------------------
if isnan(eventdata.NewData) 
    warndlg('Input is not numeric. Please enter a numeric value.');
    update_Data(handles);
else
    % idx = [row col] selected
    idx = eventdata.Indices;
    % Station edit (column == 1)
    if idx(2) == 1
        if isempty(manualStationStage)
            manualStationStage = [eventdata.NewData SS(idx(1), 2) 1];
        else
            manualStationStage(end + 1,:) = [eventdata.NewData SS(idx(1), 2) size(handles.manualStationStage,1)+1];
        end
    % Stage edit (column == 2)   
    elseif idx(2) == 2
        if isempty(manualStationStage)
            manualStationStage = [SS(idx(1),1) eventdata.NewData 1];
        else
            manualStationStage(end + 1,:) = [SS(idx(1),1) eventdata.NewData 1 size(handles.manualStationStage,1)+1];
        end
    end
    
    % Delete row from the SS matrix
    SS(idx(1),:) = [];
    
    %------------------------Resize the Gage Ht Array-------------------------
    [adcpStationStage csvStationStage ~] = separate_Data(SS);
    
    if ~isempty(adcpStationStage)
        adcpGageHtVal = adcpGageHt(1);
        adcpGageHtVal = repmat(adcpGageHtVal,[size(adcpStationStage,1) 1]);
        % Assign new Gage Height lengths
        adcpGageHt = adcpGageHtVal;
    elseif ~isempty(csvStationStage)
        csvGageHtVal = csvGageHt(1);
        csvGageHtVal = repmat(csvGageHtVal,[size(csvStationStage,1) 1]);
        % Assign new Gage Height lengths
        csvGageHt = csvGageHtVal;
    end
    
    %--------------------Assign Handles to Local Variables-------------------
    handles.SS = SS;
    handles.adcpGageHt = adcpGageHt;
    handles.csvGageHt = csvGageHt;
   
    manualStationStage(:,3) = 3;
    SS = [SS; manualStationStage];
    handles.SS = SS;

    % Call update_Data function
    handles=update_Data(handles);

    % Data has changed set save stage area rating menu option off
    set(handles.stageAreaRatingFile,'Enable','off');
    
    % Disable view stage area table
    if ishandle(handles.f)
        set(handles.f,'Visible','off');
    end
    set(handles.stageAreaRatingTable,'Enable','off');

    % Clear stage area rating graph
    cla(handles.axesStageArea);
    
    % Update handles structure
    guidata(hObject, handles);
end

function tableStationStage_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tableStationStage (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

% Obtain 1st column of current selection indices from event data;
% this contains row indices. Don't need column indices.

selection = eventdata.Indices(:,1)

% Remove duplicate row IDs
selection = unique(selection);

% Assign the selected rows to a handle
handles.selectedRows = selection;

% Set the Select and Delete buttons
set(handles.tableStationStage,'SelectionHighlight','on');

% Update handles structure
guidata(hObject, handles);
drawnow;

function pbAdd_Callback(hObject, eventdata, handles)
% hObject    handle to pbAdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%-------------------------Assign Local Variables to Handles----------------
% Station Stage Matrix
SS = handles.SS;

%--------------------------Get Station Stage Values ------------------------
prompt = {'Enter New Station:', 'Enter New Stage:'};
dlg_title = 'Add New Station and Stage';
num_lines = 1;
defaultanswer = {'0','0'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultanswer);
if ~isempty(answer)
    % Add data to end of manualStation and manualStage array and create index
    % in column 3
    manualStationStage=handles.manualStationStage;
    if isempty(manualStationStage)
        manualStationStage = [str2num(answer{1}) str2num(answer{2}) 1];
    else
        manualStationStage(end + 1,:) = [str2num(answer{1}) str2num(answer{2}) size(handles.manualStationStage,1)+1];
    end

    %----------------------------Add Data Type--------------------------------
    % Add a 3rd column for a data type of 3 to the manualStationStage
    manualStationStage(:,3) = 3;

    %-----------------------Add to Station Stage Matrix ----------------------
    % Add the manualStationStage to the Station Stage Matrix (SS)
    SS = [SS; manualStationStage];

    %----------------------Assign Handles to Local Variables ------------------
    handles.SS = SS;

    % Call update_Data function
    handles=update_Data(handles);
end
% Data has changed set save stage area rating menu option off
set(handles.stageAreaRatingFile,'Enable','off');
    
% Disable view stage area table
    if ishandle(handles.f)
        set(handles.f,'Visible','off');
    end
set(handles.stageAreaRatingTable,'Enable','off');

% Clear stage area rating graph
cla(handles.axesStageArea);
% Update handles structure
guidata(hObject, handles);

function pbDelete_Callback(hObject, eventdata, handles)
% hObject    handle to pbDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%---------------------Assign Local Variables to Handles-------------------
% Station Stage Matrix
SS = handles.SS;
adcpGageHt = handles.adcpGageHt;
csvGageHt = handles.csvGageHt;

%---------------------Delete Selected Rows--------------------------------
% Get indices to be deleted
idx_sel = handles.selectedRows;
if ~isnan(idx_sel)
    % Delete row(s) from the SS matrix
    SS(idx_sel,:) = [];

    %------------------------Resize the Gage Ht Array-------------------------
    [adcpStationStage csvStationStage ~] = separate_Data(SS);

    if ~isempty(adcpStationStage)
        adcpGageHtVal = adcpGageHt(1);
        adcpGageHtVal = repmat(adcpGageHtVal,[size(adcpStationStage,1) 1]);
        % Assign new Gage Height lengths
        adcpGageHt = adcpGageHtVal;
    elseif ~isempty(csvStationStage)
        csvGageHtVal = csvGageHt(1);
        csvGageHtVal = repmat(csvGageHtVal,[size(csvStationStage,1) 1]);
        % Assign new Gage Height lengths
        csvGageHt = csvGageHtVal;
    end

    %--------------------Assign Handles to Local Variables-------------------
    handles.SS = SS;
    handles.adcpGageHt = adcpGageHt;
    handles.csvGageHt = csvGageHt;

    %-----------------------------Update GUI---------------------------------
    % Call the update_Data function

    handles=update_Data(handles);

    % Clear/reset the handles for the selected rows and turn the Delete button
    % off.
    handles.selectedRows = nan;

    % Data has changed set save stage area rating menu option off
    set(handles.stageAreaRatingFile,'Enable','off');
    
    % Disable view stage area table
    if ishandle(handles.f)
        set(handles.f,'Visible','off');
    end
    set(handles.stageAreaRatingTable,'Enable','off');

    % Clear stage area rating graph
    cla(handles.axesStageArea);
end
% Update handles structure
guidata(hObject, handles);

% STAGE AREA RELATIONSHIP PANEL
%-------------------------------------------------------------------------
function txtBoxMaxStage_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxMaxStage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxMaxStage as text
%        str2double(get(hObject,'String')) returns contents of
%        txtBoxMaxStage as a double

% Make sure user enters a numeric value that is larger than the minStage
% Set the txtBoxSelected to 1 so that the default values will not be used
maxStage = str2double(get(handles.txtBoxMaxStage,'String'));
minStage = str2double(get(handles.txtBoxMinStage,'String'));
if isnan(maxStage) || maxStage < minStage
    warndlg('Input is either not numeric or smaller than the minimum stage. Please enter a numeric value larger than minimum stage.');
    set(handles.txtBoxMaxStage,'String',nanmax(handles.tableData(:,2)));
end

% Update handles structure
guidata(hObject, handles);

function txtBoxMinStage_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxMinStage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxMinStage as text
%        str2double(get(hObject,'String')) returns contents of
%        txtBoxMinStage as a double

% Make sure user enters a numeric value that is smaller than the maxStage
% Set the txtBoxSelected to 1 so that the default values will not be used
maxStage = str2double(get(handles.txtBoxMaxStage,'String'));
minStage = str2double(get(handles.txtBoxMinStage,'String'));
if isnan(minStage) || minStage > maxStage
    warndlg('Input is either not numeric or larger than the maximum stage. Please enter a numeric value smaller than maximum stage.');
    set(handles.txtBoxMinStage,'String',nanmin(handles.tableData(:,2)));
end

% Update handles structure
guidata(hObject, handles);

function txtBoxIncrement_Callback(hObject, eventdata, handles)
% hObject    handle to txtBoxIncrement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBoxIncrement as text
%        str2double(get(hObject,'String')) returns contents of
%        txtBoxIncrement as a double

% Make sure user enters a numeric value that is greater than zero
% Set the txtBoxSelected to 1 so that the default values will not be used
increment = str2double(get(handles.txtBoxIncrement,'String'));
if isnan(increment) || increment <= 0
    warndlg('Input is either not numeric, zero, or less than zero. Please enter a numeric value greater than zero.');
    set(handles.txtBoxIncrement,'String',0.1);
end

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pbSetDefault.
function pbSetDefault_Callback(hObject, eventdata, handles)
% hObject    handle to pbSetDefault (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%------------------Assign Handles to Local Variables ----------------------
SS = handles.SS;

% Set the text boxes with the default values of the max and min stage
% values in the StationStageMatrix and use 0.1 as the increment.
set(handles.txtBoxMaxStage,'String',num2str(max(SS(:,2))))
set(handles.txtBoxMinStage,'String',num2str(min(SS(:,2))))
set(handles.txtBoxIncrement,'String',num2str(0.1))

function pbComputeStageArea_Callback(hObject, eventdata, handles)
% hObject    handle to pbComputeStageArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%-------------------------Assign Local Variables to Handles---------------
SS = handles.SS;

%------------------------- Get Parameters -------------------------------
% Get the values from the Max Stage, Min Stage, and Increment text boxes
maxStage = str2double(get(handles.txtBoxMaxStage,'String'));
minStage = str2double(get(handles.txtBoxMinStage,'String'));
increment = str2double(get(handles.txtBoxIncrement,'String'));

%------------------------- Check Inputs ---------------------------------
% Get the maximum stage from SS and compare to the maxStage entered
% by the user. If the maxStage enterd by the user is larger than the 
% highest stage value, then display a warning telling user that a vertical
% wall will be assumed on in the stage area calculation.
SS_bank1 = SS(1,2);
SS_bank2 = SS(end,2);
% Round to second decimal place
bank1 = round(SS_bank1*100);
bank1 = bank1/100;
bank2 = round(SS_bank2*100);
bank2 = bank2/100;

if maxStage > bank1 || maxStage > bank2
   f = warndlg('"Max Stage" input is larger than one of the bank elevations. A vertical wall will be assumed for the stage-area computation.');
   uiwait(f);
end

%------------------------ Create Increment Array ------------------------
% Create an array containing the minimum stage and maximum stage with
% incremented stages between.
stageIncrements = [minStage:increment:maxStage maxStage]';

% If the next to last element in the stage increment array equals the maximum 
% stage value, then the last two elements in the index_array will be equal 
% to the maximum stage value.  Accept only one maximum stage value.
if stageIncrements(end - 1) == maxStage
    stageIncrements = stageIncrements(1:end-1);
end

%------------------------ Create New Points If Needed --------------------
% Get the current Station and Stage values from SS matrix
% If the user input for max stage is greater than the first and last stage
% points in the table, then create a very near vertical line by adding  
% a very small number (dx) to the last station point and subtracting a
% very small number (dx) from the first station point.
% user input for max stage.
stations = SS(:,1);
stages = SS(:,2);
dx = 0.0001;
% If the first stage value in the table is smaller than the maxStage, then 
% create a new point that has a station value shifted by dx and a stage
% value equal to the maxStage.
if stages(1) < maxStage
    stations = [stations(1)-dx; stations];
    stages = [maxStage; stages];
end
% If the last stage value in the table is smaller than the maxStage, then 
% create a new point that has a station value shifted by dx and a stage
% value equal to the maxStage.
if stages(end) < maxStage
    stations = [stations; stations(end)+dx];
    stages = [stages; maxStage];
end
 
% %------------------------- Compute Area ----------------------------------
% Create an array to hold an area value at each stage increment
areas = zeros(length(stageIncrements)-1, 1);

plot_CrossSection(handles.SS, handles.adcpGageHt, handles.csvGageHt, handles.axesCrossSection)

% Compute area under highest stage increment (stageIncrements(sI+2)) and
% area under lowest stage increment (stageIncrements(1)).
sI=length(stageIncrements)-2;
areas(sI+2) = compute_Area(handles, stageIncrements(sI+2),minStage,stations,stages,1);
areas(1) = compute_Area(handles, stageIncrements(1),minStage,stations,stages,-1);

hwait=waitbar(0,'Computing Stage Area'); 
for i = 1:sI
    waitbar(i./sI,hwait);
    areas(i+1) = compute_Area(handles, stageIncrements(i+1),minStage,stations,stages,0);
end
   
close(hwait);

%--------------------Call plot_StageArea Function ------------------------
plot_StageArea(areas, stageIncrements, handles.axesStageArea)

%------------------------- Assign Handles --------------------------------
% Make handles to area and stage data
handles.areas = areas;
handles.stageIncrements = stageIncrements;

% Turn save menu option on
set(handles.stageAreaRatingFile,'Enable','on');
set(handles.stageAreaRatingTable,'Enable','on');
if ishandle(handles.f)
    stageAreaRatingTable_Callback(hObject, eventdata, handles)
end

% Update handles structure
guidata(hObject, handles);




%=========================================================================
%                              FUNCTIONS 
%=========================================================================

%************************** DATA HANDLING ********************************

% SEPARATE DATA                            
%-------------------------------------------------------------------------
function [adcpStationStage, csvStationStage, manualStationStage] = separate_Data(StationStageMatrix)

% Function separate_Data takes the combined station stage table and 
% separates it basing on the data type: 1 => adcp data
%                                       2 => csv data
%                                       3 => manual data

% Find indices of the data based on data type using logical indexing
idxADCP = StationStageMatrix(:,3) == 1;
idxCSV = StationStageMatrix(:,3) == 2;
idxMANUAL = StationStageMatrix(:,3) == 3;

% Extract out proper station stage data based on indices
adcpStationStage = StationStageMatrix(idxADCP,:);
csvStationStage = StationStageMatrix(idxCSV,:);
manualStationStage = StationStageMatrix(idxMANUAL,:);


% COMBINE DATA                           
%-------------------------------------------------------------------------
function newSS = combine_Data(adcpStationStage,csvStationStage,manualStationStage)

% Function combine_Data takes the altered ADCP, CSV, and manual station 
% and stage data and combines it into one matrix

newSS = [adcpStationStage; csvStationStage; manualStationStage];


% UPDATE DATA                           
%-------------------------------------------------------------------------
function [handles]=update_Data(handles)

% The function update_Data updates Station Stage Table, Cross Section
% Plot, and the max, min, and increment values for the Stage Area 
% Relationship panel which are based on the current Station Stage Table.

% ---------------------Assign Local Variables----------------------------
% Station Stage Matrix
SS = handles.SS;
adcpGageHt = handles.adcpGageHt;
csvGageHt = handles.csvGageHt;

%--------------------------- Sort Data -----------------------------------
% Sort the data based on station
SS = sortrows(SS,1);

%--------------------------- Set Table --------------------------------
% Set the combined and sorted station stage data without the data type
% in the table.  First column = station; Second column = stage
set(handles.tableStationStage,'Data',SS(:,1:2));

% ---------------------Assign Handles to Local Variables-------------------
% Handle to combined and sorted data
handles.SS = SS;

%--------------------- Update Cross Section Plot --------------------------
% Plot cross section with gage height
plot_CrossSection(SS,adcpGageHt,csvGageHt,handles.axesCrossSection)



%****************************** COMPUTING *********************************

% COMPUTE ADCP STATION STAGE                        
%--------------------------------------------------------------------------
function [stationStage, projxy, gageHt] = compute_adcpStationStage(btTrack,depth)

% compute_adcpStationStage function
%       inputs: btTrack -> (xcoord,ycoord) => bottom track, depth, handles structure
%       outputs: array of station and stage values, projected x,y points,
%       and gage height
%
% Procedure:
%            1. Compute *stationing* by projecting each bottom track point 
%               onto a unit vector using the unit vector method. 
%               Distances to each projected point are then calculated to
%               give the final station values.
%            2. Compute *stages* by subtracting the adcp depth values from
%               a default gage height of zero.
%            3. Create a data type for the ADCP data -> data type = 1
%            4. Put the stations, stages, and data type together in a matrix
%            5. Sort the matrix based on the stationing so that the
%               stationing increases from smallest to largest.
%
%----------------------- Compute Stationing ------------------------------
% Unit vector method - project track points onto a unit vector that 
% extends from the origin to the last track point. 
%       projxy - matrix of projected x,y points 

% Unit Vector
unitVec = btTrack(end,:)./norm(btTrack(end,:));

% Dot Product
dotprod = dot(btTrack, repmat(unitVec, size(btTrack,1),1),2);

% Projection = Dot Product * Unit Vector
projxy = repmat(dotprod,1,size(unitVec,2)).*repmat(unitVec,size(dotprod,1),1);

% Compute distance from origin to each projected x,y point.
station = sqrt(projxy(:,1).^2 + projxy(:,2).^2);

%------------------------ Compute Stages ---------------------------------
% Make an gage ht array with a default value of zero to compute initial stage.
gageHt = zeros(length(station),1);
% Subtract the depth values from the gageHt value
stage = gageHt - depth;

%------------------------ Assign Data Type --------------------------------
% Assign a data type of 1 to the ADCP data
dataType = ones(length(station),1);

%------------------- Combine Station, Stage, Data Type --------------------
stationStage = [station, stage, dataType];
% Sort stationStage matrix by column 1 (station).
stationStage = sortrows(stationStage,1);


% COMPUTE CSV STATION STAGE                         
%--------------------------------------------------------------------------
function [csvStationStage, csvGageHt] = compute_csvStationStage(csvStationDepth,handles)

% compute_csvStationStage function
%       inputs: csvStationDepth file and handles structure
%       save: array of station and stage values and gage height values
%
% The function compute_csvStationStage assigns an array of station values
% from the csvStationDepth file.
% The function compute_StationStage computes an array of
% stage values by subtracting the csv depth values from the gage height 
% text box in the CSV panel.

% The parameter "stageFactor" exitst to allow user to import a csv file of 
% surveyed elevations directly without have to compute depths.
% csvStageFlag = +1 if the csv file contains elevations
% csvStageFlag = -1 if the csv file contains depths

% csvStageFlag = +1 => elevations => stage = gage ht + elevations
% csvStageFlag = -1 => depths => stage = gage ht - depths

%------------------------ Compute Stages ---------------------------------
% Make an gage ht array with a default value of zero to compute initial stage.
csvGageHt = zeros(size(csvStationDepth,1),1);

% Compute the stage; add if csv file has elevations, and subtract if csv
% file has depths. Depths are in second column of csvStationDepth matrix
if handles.csvStageFlag == 1
    csvStages = csvGageHt + csvStationDepth(:,2);
else
    csvStages = csvGageHt - csvStationDepth(:,2);
end

%------------------------ Assign Data Type --------------------------------
% Assign a data type of 2 to the CSV data
csvDataType = zeros(size(csvStationDepth,1),1) + 2;

%------------------- Combine Station, Stage, Data Type --------------------
csvStationStage = [csvStationDepth(:,1), csvStages, csvDataType];

% Sort stationStage matrix by column 1 (station).
csvStationStage = sortrows(csvStationStage,1);


% COMPUTE ACT STATION STAGE                         
%--------------------------------------------------------------------------
function [actStationStage, actGageHt] = compute_actStationStage(actStationElev, actGageHt, handles)

%------------------------ Compute Stages ---------------------------------
% Make an gage ht array same size as input.
actGageHt = repmat(actGageHt, [size(actStationElev,1), 1]);

% AREACOMP1.3 Files contain elevations => add to gage height
actStages = actGageHt + actStationElev(:,2);

%------------------------ Assign Data Type --------------------------------
% Assign a data type of 2 to the ACT data
actDataType = zeros(size(actStationElev,1),1) + 2;

%------------------- Combine Station, Stage, Data Type --------------------
actStationStage = [actStationElev(:,1), actStages, actDataType];

% Sort stationStage matrix by column 1 (station).
actStationStage = sortrows(actStationStage,1);


% COMPUTE AREA (STAGE AREA RELATIONSHIP)                    
%--------------------------------------------------------------------------
function areas = compute_Area(handles,stageIncVal,minStage,stations,stages,toggle)

% Function compute_Area computes the area of under the line at a height
% equal to the increment and between the intersection points of the 
% increment line and the line connecting consecutive (station, stage) 
% points.  The intersection points are found using the interpolation
% function interp1.  Logical indexing is used to find the station and stage
% points that lie under the increment line.  An array called "check" is used
% to assign stage points above the increment line a value of 0 and stage
% points below the increment line a value of 1.  Next, an array called 
% "checkDiff" is used to locate where the stage points cross from above the
% increment to below the increment line.  An index array called "intIdx" is
% used to hold the index where the stage points cross from above the
% increment line to below the increment line.  Next, the array's called
% "useStation" and "useStage" are used to hold the stations and stages 
% that lie from the first intersection point to last intersection point.
% Next, an array called "stationStage" is created to hold the station and
% stage values in the proper order.  The area of interest is computed by
% taking the difference between the rectangular area under the increment
% line and the polygonal area under the stage points.

useStage=[];
useStation=[];
check=ones(size(stages));
check(stages>stageIncVal)=0;
checkDiff=diff(check);
intIdx=find(checkDiff~=0);
for j=1:length(intIdx)
    useStation(j,1)=interp1(stages(intIdx(j):intIdx(j)+1),stations(intIdx(j):intIdx(j)+1),stageIncVal);
    useStage(j,1)= stageIncVal;
end
useStation=[useStation ;stations(logical(check))];
useStage=[useStage; stages(logical(check))];
stationStage=[useStation useStage];
stationStage=sortrows(stationStage,1);
if size(stationStage,1)>0
    areas=trapz([stationStage(1,1); stationStage(end,1)],[stageIncVal; stageIncVal]) - trapz(stationStage(:,1),stationStage(:,2));
else
    areas=0;
end

% Separate Data for plotting
[adcpStationStage, csvStationStage, ~] = separate_Data(handles.SS);

% Toggle is used for plotting; +1 => Plot area under the last (largest) stage
% increment (Max Stage); -1 => Plot area under the smallest stage increment (Min Stage);
% 0 => Do nothing.
if toggle==-1
    axes(handles.axesCrossSection)
    hold on
    fill(stationStage(:,1),stationStage(:,2),[0.5 0.5 0.5],'LineStyle','none');
    
elseif toggle==1   
    axes(handles.axesCrossSection)
    hold on
    fill(stationStage(:,1),stationStage(:,2),'g','LineStyle','none')
    % Plot the GageHt line over the filled area.
    if isempty(adcpStationStage)
        plot(csvStationStage(:,1), handles.csvGageHt,'r-','LineWidth',2);
    else
        plot(adcpStationStage(:,1), handles.adcpGageHt,'b-','LineWidth',2);
    end
end



%****************************** PLOTTING **********************************

% PLOT BOTTOM TRACK 
%--------------------------------------------------------------------------
function plot_BottomTrack(btTrack,projxy,idxTrackPlot,axesTrack)

% The function plot_BottomTrack plots the adcp track across the river

% Legend Items
legendItems = {'ADCP Track','Projection Line','Projected Points','Perpendicular Lines'};

% Reset the plot and use the proper axes
cla(axesTrack,'reset');
axes(axesTrack)

% Make the first element of idxTrackPlot true in order to always plot the
% adcp track.
idxTrackPlot(1) = true;

% Four plots on the bottom track plot axes. Initialize plots as 'off'
% 1. ADCP Track - x,y points
h(1) =  plot(btTrack(:,1), btTrack(:,2), '*');
set(h(1),'Visible','off')

% 2. Projection Line
hold on
h(2) = plot([0 btTrack(end,1)], [0 btTrack(end,2)],'k-');
set(h(2),'Visible','off')

% 3. Projected Points
hold on
h(3) = plot(projxy(:,1), projxy(:,2), 'g.');
set(h(3),'Visible','off')

% 4. Perpendicular Lines
hold on
for i = 1:length(projxy)
    h(4) = plot([btTrack(i,1) projxy(i,1)], [btTrack(i,2) projxy(i,2)], 'r-');
    set(h(4),'Visible','off')
end

% Read the current indices and plot accordingly
if ~isempty(h(idxTrackPlot))
    axes(axesTrack)
    % Have to turn on each perpendicular line.  Unfortunately, have to
    % re-plot each line too.
    if idxTrackPlot(4) == true
        for i = 1:length(projxy)
            h(4) = plot([btTrack(i,1) projxy(i,1)], [btTrack(i,2) projxy(i,2)], 'r-');
            set(h(4),'Visible','on')
        end
    end
    set(h(idxTrackPlot),'Visible','on')
    % Labels and Legend
    xlabel('X coordinate');
    ylabel('Y coordinate');
    title('ADCP Track');
    axis equal
    legend(h(idxTrackPlot), legendItems(idxTrackPlot));
end


% PLOT CROSS SECTION 
%-------------------------------------------------------------------------
function plot_CrossSection(StationStageMatrix, adcpGageHt, csvGageHt, axesCrossSection)

% The function plot_CrossSection plots station vs. stage and station vs.
% gageHt.

%------------------------ Separate Station Stage Data ---------------------
% Call the separate_Data function to extract out proper data
[adcpStationStage, csvStationStage, manualStationStage] = separate_Data(StationStageMatrix);


%------------------------Update Index Array ------------------------------
% Update the following index array for the cross section plot legend:
% [(1) ADCP Gage Ht
%  (2) CSV Gage Ht
%  (3) ADCP Stage 
%  (4) Manual Stage
%  (5) CSV Stage]
idxCrossSectionPlot(1) = false;
idxCrossSectionPlot(2) = false;
idxCrossSectionPlot(3) = false;
idxCrossSectionPlot(4) = false;
idxCrossSectionPlot(5) = false;
p = nan(1,5);

% Legend Items
legendItems = {'ADCP Gage Height','CSV Gage Height','Stage','Manual Stage','CSV Stage'};

% Reset the plot and use the proper axes
cla(axesCrossSection,'reset');
axes(axesCrossSection)

% Four plots on the bottom track plot axes. Initialize plots as 'off'.

% Plot the full cross section which contains all the data
p(3)=plot(StationStageMatrix(:,1),StationStageMatrix(:,2),'-k','LineWidth',2);

% Station vs. ADCP gage ht
if ~isempty(adcpStationStage)
    hold on
    p(1)=plot(adcpStationStage(:,1),adcpGageHt,'b-','LineWidth',2);        
    idxCrossSectionPlot(1) = true;
    idxCrossSectionPlot(3) = true;
end

% Station vs. CSV Gage Ht
if ~isempty(csvStationStage)
    hold on
    p(5)=plot(csvStationStage(:,1), csvStationStage(:,2),'*g','LineWidth',2);
    p(2)=plot(csvStationStage(:,1),csvGageHt,'r-','LineWidth',2); 
    idxCrossSectionPlot(2) = true;
    idxCrossSectionPlot(5) = true;
end
% Station vs. Manual Stage
if ~isempty(manualStationStage)
    hold on
    p(4) = plot(manualStationStage(:,1), manualStationStage(:,2),'ok'); 
    idxCrossSectionPlot(4) = true;
end

% Find the axis parameters
maxX = max(StationStageMatrix(:,1)) + 1;
minX = min(StationStageMatrix(:,1)) - 1;

maxY = max(cat(1,StationStageMatrix(:,2), adcpGageHt, csvGageHt)) + 6;
minY = min(StationStageMatrix(:,2)) - 2;

% Labels and Legend
xlabel('Station');
ylabel('Stage');
title('Cross Section');
axis([minX maxX minY maxY])
legend(p(idxCrossSectionPlot), legendItems(idxCrossSectionPlot));


% PLOT STAGE AREA 
%-------------------------------------------------------------------------
function plot_StageArea(areas, stageIncrements, axesStageArea)

% The function plot_StageArea plots the computed areas vs. stage in the
% axes called axesStageArea

% Plot area vs. stage
% stageIncrement(1) = Min Stage value in the Stage Area Relationship panel
axes(axesStageArea)
plot(areas,stageIncrements,'*-');
xlabel('Area');
ylabel('Stage');
axis([min(areas)-10 max(areas)+10 min(stageIncrements)-.25 max(stageIncrements)+.25]);



%********************** SAVING AND CLEARING GUI **************************

% SAVE WORKSPACE 
%-------------------------------------------------------------------------
function handles = saveWorkspace(handles)

% This function asks the user to save the current workspace. User supplies
% the name of the saved work, and the saved work is placed in the same
% directory as the input ('Import') data

%-------------------- Assign Local Variables to Handles-------------------
% Original ADCP and CSV data
btTrack = handles.btTrack;
projxy = handles.projxy;
adcpDepth = handles.adcpDepth;
adcpStationStageOrig = handles.adcpStationStageOrig;
csvStationDepth = handles.csvStationDepth;
csvStationStageOrig = handles.csvStationStageOrig;

% User edited station stage (SS) matrix
SS = handles.SS;
adcpGageHt = handles.adcpGageHt;
csvGageHt = handles.csvGageHt;
adcpStationOffset = handles.adcpStationOffset;
csvStationOffset = handles.csvStationOffset;

% Text boxes
userDate = get(handles.txtBoxDate,'String');
userStationNumber = get(handles.txtBoxStationNumber,'String');
userName = get(handles.txtBoxUserName,'String');
userComments = get(handles.txtBoxComments,'String');

maxStage = str2double(get(handles.txtBoxMaxStage,'String'));
minStage = str2double(get(handles.txtBoxMinStage,'String'));
increment = str2double(get(handles.txtBoxIncrement,'String'));
adcpFileName = get(handles.txtAdcpFilename,'String');
csvFileName = get(handles.txtCSVFilename,'String');

% Plotting variables
idxTrackPlot = handles.idxTrackPlot;

% Create a matlab file and save necessary data to it.
[FileName, PathName] = uiputfile('*.mat','Save Current Work As',handles.PathName);

% If user clicks "Cancel", uigetfile returns 0.
if isequal(FileName,0)
   return
end

handles.workspaceOut = [PathName FileName];
fullname = handles.workspaceOut;
save(fullname, 'btTrack','projxy','adcpDepth','adcpStationStageOrig','csvStationDepth',...
               'csvStationStageOrig','SS','adcpGageHt','csvGageHt',...
               'userDate','userStationNumber','userName','userComments',...
               'adcpGageHt','adcpStationOffset','csvGageHt','csvStationOffset',...
               'maxStage','minStage','increment','adcpFileName','csvFileName','idxTrackPlot');


% CLEAR GUI 
%-------------------------------------------------------------------------
function handles=clear_gui(handles)

% The function clear_gui asks is used to clear all the data in the GUI
% before opening a new data file.
c=questdlg('Clear all data before loading new data?','Clear GUI','Yes','No','Yes');
if strcmp(c,'Yes')
    % Clear plots
    cla(handles.axesStageArea);
    cla(handles.axesTrack);
    cla(handles.axesCrossSection);
    % Clear text boxes
    set(handles.txtBoxDate,'String',' ');
    set(handles.txtBoxStationNumber,'String',' ');
    set(handles.txtBoxUserName,'String',' ');
    set(handles.txtBoxComments,'String',' ');
    set(handles.tableStationStage,'Data','');
    set(handles.txtBoxGageHt,'String','0');
    set(handles.txtBoxStationOffset,'String','0');
    set(handles.txtBoxCSVGageHt,'String','0');
    set(handles.txtBoxCSVStationOffset,'String','0');
    set(handles.txtBoxMaxStage,'String',' ');
    set(handles.txtBoxMinStage,'String',' ');
    set(handles.txtBoxIncrement,'String',' ');
    set(handles.txtAdcpFilename,'String',' ');
    set(handles.txtCSVFilename,'String',' ');
    
    set(handles.tableStationStage,'Enable','off');
    set(handles.txtBoxGageHt,'Enable','off');
    set(handles.txtBoxStationOffset,'Enable','off');
    set(handles.txtBoxCSVGageHt,'Enable','off');
    set(handles.txtBoxCSVStationOffset,'Enable','off');
    
    % Clear data arrays and matrices
    handles.SS = [];
    handles.btTrack = [];
    handles.projxy = [];
    handles.adcpGageHt = [];
    handles.csvStationDepth = [];
    handles.csvGageHt = [];
    handles.manualStationStage = [];
    handles.adcpStationOffset = 0;
    handles.csvStationOffset = 0;   
    handles.idxTrackPlot(1:4) = false;
    handles.idxTrackPlot(2) = true; 

end








%=========================================================================
%                       Unused Functions
%=========================================================================
% --- Executes during object creation, after setting all properties.
function txtBoxMaxStage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxMaxStage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function txtBoxMinStage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxMinStage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function txtBoxIncrement_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxIncrement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function txtBoxCSVGageHt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxCSVGageHt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function txtBoxCSVStationOffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxCSVStationOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function txtBoxGageHt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxGageHt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function txtBoxStationOffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxStationOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function txtBoxDate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function txtBoxStationNumber_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxStationNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function txtBoxUserName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxUserName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function txtBoxComments_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBoxComments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
